﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIBase : MonoBehaviour
{
    [SerializeField]
    protected UIView uIViewParent;
    public static Action<UIView> CallView;
    public virtual void OpenView()
    {
        CallView(uIViewParent);
        CloseView();
    }

    private void CloseView()
    {
        Destroy(this.gameObject);
    }
}
