﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IUManager : MonoBehaviour
{
    private void Awake()
    {
        Sample4_FactoryView.CreateView(UIView.Main, this.transform);
        UIBase.CallView += OnCallView;
    }

    private void OnCallView(UIView view)
    {
        Sample4_FactoryView.CreateView(view, this.transform);
    }




}
